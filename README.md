Course project
==============

### Deployment

To run the application you can use docker.  
Just clone the repository and in the root folder run:

`docker-compose build`

`docker-compose up`

Then navigate to http://localhost