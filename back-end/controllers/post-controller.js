const postRepository = require('../repositories/post-repository');

exports.getPosts = (req, res) => {
    let queryThenable;
    let resPosts;
    if(req.query.pageNumber && req.query.pageLength){
        queryThenable = postRepository.getPagedPosts(+req.query.pageNumber, +req.query.pageLength);
    } else {
        queryThenable = postRepository.getPosts();
    }

    queryThenable
    .then(posts => {
        resPosts = posts
        return postRepository.getCount()
    })
    .then(count => {
        response = {
            count: count,
            posts: resPosts
        }
        res.status(200).json(response);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err});
    });
};

exports.getPost = (req, res) => {
    postRepository.getPost(req.params.id)
        .then(post => {
            res.status(200).json(post);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
};

exports.addPost = (req, res) => {
    postRepository.addPost(req.body.title, req.body.content, req.userData, req.file)
        .then(post => {
            res.status(201).json(post);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
};

exports.deletePost = (req, res) => {
    postRepository.checkPostPermissions(req.params.id, req.userData.id)
        .then(authorized => {
            if(authorized){
                return postRepository.deletePost(req.params.id);
            } else{
                throw 'notAuthorized';
            }
        })
        .then(() => {
            console.log('item with id: ' + req.params.id + ' deleted');
            res.status(202).send();
        })
        .catch(err => {
            console.log(err);
            if(err === 'notAuthorized'){
                res.status(401).json({error: err});
            } else {
                res.status(500).json({error: err});
            }
        });
};

exports.updatePost = (req, res) => {
    const newPostData = req.body;
    postRepository.checkPostPermissions(req.body._id, req.userData.id)
        .then(authorized => {
            if(authorized){
                return postRepository.updatePost(newPostData, req.file);
            } else{
                throw 'notAuthorized';
            }
        })
        .then(post => {
            console.log('item with id: ' + newPostData._id + ' updated');
            res.status(202).send(post);
        })
        .catch(err => {
            console.log(err);
            if(err === 'notAuthorized'){
                res.status(401).json({error: err});
            } else {
            res.status(500).json({error: err});
            }
        });
};