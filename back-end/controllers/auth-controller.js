const userRepo = require('../repositories/user-repository');

exports.login = (req, res) => {
    userRepo.login(req.body.email, req.body.password)
        .then(data => {
            if(data.token && data.userId){
                res.status(200).json({token: data.token, expiresIn: 3600, userId: data.userId});
            } else {
                throw 'unauthorized';
            }
        })
        .catch(err => {
            console.log(err);
            res.status(401).send();
        });
}

exports.signup = (req, res) => {
    userRepo.signup(req.body.email, req.body.password)
     .then(() => {
         res.status(201).send();
     })
     .catch(err => {
         console.log(err);
         res.status(500).json({err: err});
     }); 
 }