const imageRepo = require('../repositories/image-repository');

exports.getImage = (req, res) => {
    imageRepo.getImage(req.params.id)
        .then(image => {
            res.contentType(image.mimeType)
                .send(image.data);
        });
};