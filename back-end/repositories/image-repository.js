const Image = require('../models/image');

module.exports = {
    addImage(fileName, mime, data) {
        const image = new Image({
            fileName: fileName, 
            mimeType: mime, 
            data: data
        });
        return image.save();
    },

    getImage(id) {
        return Image.findById(id);
    }
}