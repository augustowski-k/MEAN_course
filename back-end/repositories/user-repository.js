const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = {
    signup(email, password) {
        return bcrypt.hash(password, 10)
            .then(hash => {
                const user = new User({
                    email: email,
                    passwordHash: hash
                })
                return user.save();
            });
    },

    login(email, password) {
        let foundUser;
        return User.findOne({ email: email })
            .then(user => {
                if(!user){
                    return null;
                }
                foundUser = user;
                return bcrypt.compare(password, user.passwordHash);
            })
            .then( isValid => {
                if (isValid) {
                    const token = jwt.sign({ email: foundUser.email, id: foundUser._id}, process.env.JWT_SECRET, {expiresIn: '1h'});
                    return {token: token, userId: foundUser._id};
                }
                return null
            });
    }
}