const Post = require('../models/post');
const imageRepo = require('./image-repository');

const processAddingImageData = function(postData, imageData) {
    if(imageData !== null && imageData !== undefined){
        return imageRepo.addImage(imageData.originalname, imageData.mimetype, imageData.buffer)
            .then(image => {
                postData.imageId = image._id;
                return postData;
            });
    } else {
        return new Promise((resolve) => {
            resolve(postData);
        });
    }
}

module.exports = {
    addPost(title, content, userData, imageData) {
        const postData = {title: title, content: content, user: userData.id};
        return processAddingImageData(postData, imageData)
            .then(processedPostData => {
                const post = new Post(processedPostData);
                return post.save();
            });
    },

    getPosts() {
        return Post.find()
            .populate('user', 'email _id');
    },

    getPagedPosts(pageNumber, pageLength) {
        return Post.find()
            .populate('user', 'email _id')
            .sort('creationTime')
            .skip(pageLength * (pageNumber -1))
            .limit(pageLength);
    },

    getPost(id) {
        return Post.findById(id)
            .populate('user', 'email _id');
    },

    getCount() {
        return Post.count();
    },

    deletePost(id) {
        return Post.findByIdAndRemove(id);
    },
    
    updatePost(post, file) {
        return processAddingImageData(post, file)
            .then(postData => {
                return Post.findByIdAndUpdate({_id: postData._id}, postData);
            });
    },

    checkPostPermissions(postId, userId) {
        return Post.findById(postId)
            .then(post => {
                return post.user.equals(userId)
            })
    }
}