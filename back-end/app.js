const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const postsRouter = require('./routes/post-routes');
const imageRouter = require('./routes/image-routes');
const authRouter = require('./routes/auth-routes');

mongoose.connect(process.env.MONGO_CONNECTION_STRING);

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE, OPTIONS')

    next();
});

app.use('/api/posts', postsRouter);
app.use('/api/imgs', imageRouter);
app.use('/api/auth', authRouter);

app.listen(process.env.PORT || 8000, '0.0.0.0', 511, () => {
    console.log('app runs on 8000');
});
