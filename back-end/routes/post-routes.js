const express = require('express');
const multer = require('multer');
const authMiddleware = require('../middleware/auth-middleware');
const postsController = require('../controllers/post-controller')

const memoryStore = multer.memoryStorage();
const upload = multer({storage: memoryStore})

const router = express.Router();

router.get('/', 
    postsController.getPosts
);

router.get('/:id', 
    postsController.getPost
);

router.post('/', 
    authMiddleware, 
    upload.single('image'), 
    postsController.addPost
);

router.delete('/:id', 
    authMiddleware, 
    postsController.deletePost
);

router.patch("/", 
    authMiddleware, 
    upload.single('image'), 
    postsController.updatePost
);

module.exports = router;