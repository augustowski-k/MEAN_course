const mongoose = require('mongoose');

const imageSchema = new mongoose.Schema({
    fileName: {type: String, required: true},
    mimeType: {type: String, required: true},
    data: {type: Buffer, required: true}
})

module.exports = mongoose.model("Image", imageSchema);