import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs/observable/of';
import { take, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { environment } from '../../../../environments/environment';



const BACKEND_URL = environment.apiUrl + '/auth/';

@Injectable()
export class AuthService {
  private token: string;
  private authenticationState = new BehaviorSubject<boolean>(false);
  private timerToken: any = null;
  private userId = new BehaviorSubject<string>(null);

  constructor(private http: HttpClient,
              private router: Router) { }

  getToken() {
    return this.token ? this.token.slice() : null;
  }

  getUserId() {
    return this.userId.asObservable();
  }

  isAuthenticated() {
    return this.authenticationState.asObservable();
  }

  initAuth() {
    const authData = this.loadAuthData();
    if (authData.authToken && authData.expirationDate && authData.userId) {
      const timeToExpire = new Date(authData.expirationDate).getTime() - new Date().getTime();
      if (timeToExpire > 0) {
        this.userId.next(authData.userId);
        this.token = authData.authToken;
        this.timerToken = setTimeout(this.logout.bind(this), timeToExpire);
        this.authenticationState.next(true);
      } else {
        this.clearAuthData();
      }
    }
  }

  signup(email, password) {
    return this.http.post(
      BACKEND_URL + 'signup', {email: email, password: password})
      .pipe(
        take(1),
        catchError((err, res) => {
          if (err) {
            console.log(err);
            return of({error: true});
          }
        })
      )
      .subscribe((res: any) => {
        if (!res || !res.error) {
          this.router.navigate(['/login']);
        }
      });
  }

  login(email, password) {
    return this.http.post<{token: string, expiresIn: number, userId: string}>(
      BACKEND_URL + 'login',
      {email: email, password: password}
    )
      .pipe(
        take(1),
        catchError((err, res) => {
          if (err) {
            console.log(err);
            return null;
          }
          return res;
        })
      )
      .subscribe(res => {
        if (res) {
          this.token = res.token;
          this.userId.next(res.userId);
          this.timerToken = setTimeout(this.logout.bind(this), res.expiresIn * 1000);
          this.storeAuthData(res.token, res.expiresIn * 1000, res.userId);
          this.authenticationState.next(true);
          this.router.navigate(['/']);
        }
      });
  }

  logout() {
    if (this.timerToken) {
      clearTimeout(this.timerToken);
      this.timerToken = null;
    }
    this.token = null;
    this.userId.next(null);
    this.clearAuthData();
    this.authenticationState.next(false);
    this.router.navigate(['/']);
  }

  private storeAuthData(token: string, timeToExpire: number, userId: string) {
    localStorage.setItem('authToken', token);
    const expirationDateString = new Date(new Date().getTime() + timeToExpire).toISOString();
    localStorage.setItem('authTokenExpDate', expirationDateString);
    localStorage.setItem('userId', userId);
  }

  private clearAuthData() {
    localStorage.removeItem('authToken');
    localStorage.removeItem('authTokenExpDate');
    localStorage.removeItem('userId');
  }

  private loadAuthData(): { authToken: string, expirationDate: string, userId: string } {
    return {
      authToken: localStorage.getItem('authToken'),
      expirationDate: localStorage.getItem('authTokenExpDate'),
      userId: localStorage.getItem('userId')
    };
  }

}
