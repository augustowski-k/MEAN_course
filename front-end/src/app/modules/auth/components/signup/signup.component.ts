import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AuthService } from '../../services/auth.service';

@Component({
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.signupForm = new FormGroup({
      'email': new FormControl(null, {validators:
        [
          Validators.required,
          Validators.email
        ]}),
      'password': new FormControl(null, {validators:
        [
          Validators.required
        ]})
    });
  }

  onSignup() {
    this.authService.signup(this.signupForm.value.email, this.signupForm.value.password);
    this.signupForm.reset();
  }
}
