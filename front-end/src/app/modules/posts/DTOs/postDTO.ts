export class PostDTO {
  public creationTime;
  public _id: string;
  public imageId: string;
  public user: {email: string, _id: string};
  constructor(public title: string, public content: string) {}
}
