import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'textFormat'
})
export class TextFormatPipe implements PipeTransform {

  transform(value: string): any {
    const replaced = value.replace(/\n/g, '<br />');
    return replaced;
  }

}
