import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { PostDTO } from '../DTOs/postDTO';
import { PostRepositoryService } from './../services/post-repository.service';

@Injectable()
export class PostResolver implements Resolve<PostDTO> {
  constructor(private postRepo: PostRepositoryService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<PostDTO> | Promise<PostDTO> | PostDTO {
    return this.postRepo.getPost(route.params.postId);
  }
}
