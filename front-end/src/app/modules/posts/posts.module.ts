import { NgModule, ModuleWithProviders } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { NewPostComponent } from './components/new-post/new-post.component';
import { PostListComponent } from './components/post-list/post-list.component';
import { PostItemComponent } from './components/post-list/post-item/post-item.component';
import { PostRepositoryService } from './services/post-repository.service';
import { CommonModule } from '@angular/common';
import { PageTurnerComponent } from './components/post-list/page-turner/page-turner.component';
import { TextFormatPipe } from './pipes/text-format.pipe';

@NgModule({
  declarations: [
    NewPostComponent,
    PostListComponent,
    PostItemComponent,
    PageTurnerComponent,
    TextFormatPipe
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class PostsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: PostsModule,
      providers: [
        PostRepositoryService
      ]
    };
  }
}
