import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { take } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';

import { PostDTO } from './../DTOs/postDTO';
import { environment } from '../../../../environments/environment';

const BACKEND_URL = environment.apiUrl + '/posts/';

@Injectable()
export class PostRepositoryService {
  private posts: PostDTO[] = [];
  private subject = new Subject<PostDTO[]>();
  private postChange = new Subject<any>();

  constructor(private http: HttpClient) { }

  addPost(title, content, image?) {
    const data = new FormData();
    data.append('title', title);
    data.append('content', content);
    if (image) {
      data.append('image', image);
    }

    this.http.post<any>(BACKEND_URL, data)
      .subscribe(resData => {
        const post = new PostDTO(title, content);
        post._id = resData._id;
        post.creationTime = resData.creationTime;
        if (resData.imageId) {
          post.imageId = resData.imageId;
        }
        this.posts.push(post);
        this.subject.next(this.posts.slice());
      });
  }

  getPosts() {
    this.http.get(BACKEND_URL)
      .subscribe((response: {posts: PostDTO[], count: number}) => {
        this.posts = response.posts;
        this.subject.next(response.posts);
      });
    return this.subject.asObservable();
  }

  getChangedPostNotifier() {
    return this.postChange.asObservable();
  }

  getPostsPage(pageNumber: number, pageLength: number) {
    const obs = new Subject<{posts: PostDTO[], count: number}>();
    const params = new HttpParams()
      .set('pageNumber', pageNumber.toString())
      .set('pageLength', pageLength.toString());
    this.http.get(BACKEND_URL, {params: params})
      .subscribe((response: {posts: PostDTO[], count: number}) => {
        this.posts = response.posts;
        obs.next(response);
        obs.complete();
      });
    return obs;
  }

  getPost(id: string) {
    return this.http.get<PostDTO>(BACKEND_URL + id)
      .pipe(
        take(1)
      );
  }

  deletePost(id) {
    this.http.delete(BACKEND_URL + id)
      .subscribe(() => {
        const index = this.posts.findIndex(p => p._id === id);
        this.posts.splice(index, 1);
        this.subject.next(this.posts);
        this.postChange.next();
      });
  }

  updatePost(post: {_id: string, title?: string, content?: string, image?: Blob}) {
    const data = new FormData();
    data.append('_id', post._id);
    data.append('title', post.title);
    data.append('content', post.content);
    if (post.image) {
      data.append('image', post.image);
    }

    this.http.patch<{title: string, content: string, imageId: string, creationTime: Date}>(BACKEND_URL, data)
      .subscribe(res => {
        this.postChange.next();
      });
  }
}
