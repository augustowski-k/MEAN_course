import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PostRepositoryService } from '../../services/post-repository.service';
import { mimeType } from '../../../../validators/mime-type.validator';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {
  postForm: FormGroup;
  imagePreviewUrl: string;

  constructor(private postRepo: PostRepositoryService,
              private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    if (this.activeRoute.snapshot.url[0].path === 'create') {
      this.initializeNewForm();
    } else if ( this.activeRoute.snapshot.url[0].path === 'edit') {
      this.initializeEditForm();
      this.activeRoute.params
        .subscribe(() => {
          this.initializeEditForm();
        });
    }
  }

  initializeNewForm() {
    this.postForm = new FormGroup({
      'title': new FormControl(null, [
        Validators.required
      ]),
      'image': new FormControl(null, {asyncValidators: [mimeType]}),
      'content': new FormControl(null, [
        Validators.required
      ])
    });
  }

  initializeEditForm() {
    this.postForm = new FormGroup({
      'title': new FormControl(this.activeRoute.snapshot.data.post.title, [
        Validators.required
      ]),
      'image': new FormControl(null, {asyncValidators: [mimeType]}),
      'content': new FormControl(this.activeRoute.snapshot.data.post.content, [
        Validators.required
      ])
    });
    this.imagePreviewUrl = 'http://localhost:8000/api/imgs/' + this.activeRoute.snapshot.data.post.imageId;
  }

  clearForm() {
    this.postForm.reset();
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.postForm.patchValue({'image': file});
    this.postForm.get('image').updateValueAndValidity();
    const fr = new FileReader();
    fr.onload = () => {
      this.imagePreviewUrl = fr.result;
    };
    fr.readAsDataURL(file);
  }

  onSubmit() {
    if (this.activeRoute.snapshot.url[0].path === 'create') {
      this.createPost();
    } else if ( this.activeRoute.snapshot.url[0].path === 'edit') {
      this.updatePost();
    }

    this.clearForm();
  }

  createPost() {
    const formValues = this.postForm.value;
    this.postRepo.addPost(formValues.title, formValues.content, formValues.image);
  }

  updatePost() {
    const postUpdateObject: {_id: string, title?: string, content?: string, image?: Blob} = {
     _id: this.activeRoute.snapshot.data.post._id,
     title: this.postForm.value.title,
     content: this.postForm.value.content,
    };
    if (this.postForm.value.image) {
      postUpdateObject.image = this.postForm.value.image;
    }

    this.postRepo.updatePost(postUpdateObject);
  }
}
