import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPostComponent } from './new-post.component';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';

describe('NewPostComponent', () => {
  let fixture: ComponentFixture<NewPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPostComponent ],
      imports: [ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPostComponent);
    fixture.detectChanges();
  });

  it('should be created', () => {
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should contain a form', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('form')).toBeTruthy();
  });

  it('should have formGroup property named postForm', () => {
    const component = fixture.debugElement.componentInstance;
    expect(component['postForm'] instanceof FormGroup).toBeTruthy();
  });

  it('should have onSubmit function', () => {
    const component = fixture.debugElement.componentInstance;
    expect(typeof component.onSubmit === 'function').toBeTruthy();
  });

  describe('form', () => {

    it('should contain title input', () => {
      const compiled = fixture.debugElement.nativeElement;
      expect(compiled.querySelector('form input[formControlName="title"]')).toBeTruthy();
    });

    it('should contain content textarea', () => {
      const compiled = fixture.debugElement.nativeElement;
      expect(compiled.querySelector('form textarea[formControlName="content"]')).toBeTruthy();
    });

    it('should contain a submit button', () => {
      const compiled = fixture.debugElement.nativeElement;
      expect(compiled.querySelector('form button[type="submit"]')).toBeTruthy();
    });
  });
});
