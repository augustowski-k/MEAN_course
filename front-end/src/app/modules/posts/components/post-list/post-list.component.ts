import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { take } from 'rxjs/operators';
import { PostDTO } from '../../DTOs/postDTO';
import { PostRepositoryService } from '../../services/post-repository.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {
  private deleteNotifierSub: Subscription;
  posts: PostDTO[] = [];
  pageNumber = 1;
  pageLength = 3;
  postCount: number;
  get isLastPage() {
    return ((this.pageNumber - 1) * this.pageLength + this.posts.length) === this.postCount;
  }

  constructor(private postRepo: PostRepositoryService) { }

  ngOnInit() {
    this.loadPage();
    this.deleteNotifierSub = this.postRepo.getChangedPostNotifier()
      .subscribe(this.loadPage.bind(this));
  }

  ngOnDestroy() {
    this.deleteNotifierSub.unsubscribe();
  }

  loadPage() {
    this.postRepo.getPostsPage(this.pageNumber, this.pageLength)
      .pipe(take(1))
      .subscribe(response => {
        this.posts = response.posts;
        this.postCount = response.count;
      });
  }

  onPageTurned(page: number) {
    this.pageNumber = page;
    this.loadPage();
  }
}
