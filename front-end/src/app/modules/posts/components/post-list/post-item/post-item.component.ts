import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { PostDTO } from '../../../DTOs/postDTO';
import { PostRepositoryService } from '../../../services/post-repository.service';
import { AuthService } from '../../../../auth/services/auth.service';

@Component({
  selector: 'app-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.css']
})
export class PostItemComponent implements OnInit {
  @Input() post: PostDTO;
  isAuthenticated: Observable<boolean>;
  userId: Observable<string>;

  constructor(private postRepo: PostRepositoryService,
              private router: Router,
              private authService: AuthService) { }

  ngOnInit() {
    this.isAuthenticated = this.authService.isAuthenticated();
    this.userId = this.authService.getUserId();
  }

  onDelete() {
    this.postRepo.deletePost(this.post._id);
  }

  onEdit() {
    this.router.navigate(['/edit', this.post._id]);
  }
}
