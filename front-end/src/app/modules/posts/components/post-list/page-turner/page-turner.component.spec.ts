import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTurnerComponent } from './page-turner.component';

describe('PageTurnerComponent', () => {
  let component: PageTurnerComponent;
  let fixture: ComponentFixture<PageTurnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTurnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTurnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
