import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-page-turner',
  templateUrl: './page-turner.component.html',
  styleUrls: ['./page-turner.component.css']
})
export class PageTurnerComponent implements OnInit {
  @Input() moreAvailable = true;
  @Output() pageTurned = new EventEmitter<number>();

  currentPage = 1;

  constructor() { }

  ngOnInit() {
  }

  onNextClicked() {
    if (this.moreAvailable) {
      this.pageTurned.emit(++this.currentPage);
    }
  }

  onPreviousClicked() {
    if (this.currentPage > 1) {
      this.pageTurned.emit(--this.currentPage);
    }
  }
}
