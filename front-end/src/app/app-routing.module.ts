import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PostListComponent } from './modules/posts/components/post-list/post-list.component';
import { NewPostComponent } from './modules/posts/components/new-post/new-post.component';
import { PostResolver } from './modules/posts/guards/post.resolver';
import { SignupComponent } from './modules/auth/components/signup/signup.component';
import { LoginComponent } from './modules/auth/components/login/login.component';

const appRoutes: Routes = [
  { path: '', component: PostListComponent, pathMatch: 'full' },
  { path: 'signup', component: SignupComponent},
  { path: 'login', component: LoginComponent},
  { path: 'create', component: NewPostComponent},
  { path: 'edit/:postId', component: NewPostComponent, resolve: {post: PostResolver}}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
    PostResolver
  ]
})
export class AppRoutingModule {

}
