import { of } from 'rxjs/observable/of';
import { AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

export function mimeType(control: AbstractControl): Observable<{[s: string]: any}> | Promise<{[s: string]: any}> {
  if (control.value !== null && control.value !== undefined) {
    const file = control.value as File;
    const fileReader = new FileReader();
    const frObs = Observable.create((observer: Observer<{[s: string]: any}>) => {
      fileReader.addEventListener('loadend', () => {
        const arr = new Uint8Array(fileReader.result).subarray(0, 4);
        const isValid = hasImageType(arr);
        if (isValid) {
          observer.next(null);
        } else {
          observer.next({invalidMimeType: true});
        }
        observer.complete();
      });
    });
    fileReader.readAsArrayBuffer(file);
    return frObs;
  } else {
    return of(null);
  }
}

function hasImageType(arr: Uint8Array) {
  let header = '';
  let isValid: boolean;
  for (let i = 0; i < arr.length; i++) {
    header += arr[i].toString(16);
  }

  switch (header) {
    case '89504e47':
      isValid = true;
      break;
    case 'ffd8ffe0':
    case 'ffd8ffe1':
    case 'ffd8ffe2':
    case 'ffd8ffe3':
    case 'ffd8ffe8':
      isValid = true;
      break;
    default:
      isValid = false;
      break;
  }
  return isValid;
}
